with open('08.in') as f:
    data = f.read()

r = 6
c = 25
x = 0
layers = []
while x < len(data):
    pic = [data[i:i+c] for i in range(x, x+c*r, c)]
    layers.append(pic)
    x += c*r


# ### Part 1
# layer = ''.join(min(layers, key=lambda l: ''.join(l).count('0')))
# print(layer.count('1') * layer.count('2'))

# 0 == black
# 1 == white
# 2 == transparent
out = []
for row in range(r):
    out.append([])
    for col in range(c):
        out[row].append('.')

for x in range(c):
    for y in range(r):
        for i in range(len(layers)):
            p = layers[i][y][x]
            if p != '2' and out[y][x] == '.':
                if p == '1':
                    out[y][x] = '#'
                elif p == '0':
                    out[y][x] = ' '
                break

print('\n'.join(''.join(o) for o in out))
