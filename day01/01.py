with open('01_input') as f:
    data = f.read().split()


def req_fuel(mass):
    return int(mass) // 3 - 2


fuel = 0
for m in data:
    curr_fuel = req_fuel(m)
    tot_fuel = curr_fuel
    while True:
        curr_fuel = req_fuel(curr_fuel)
        if curr_fuel < 0:
            break
        tot_fuel += curr_fuel
    fuel += tot_fuel

print(fuel)
