from itertools import permutations
from vm import IntCode

with open('07.in') as f:
    data = list(map(int, f.read().split(',')))

init_phase = permutations([0, 1, 2, 3, 4])
feedback_phase = permutations([5, 6, 7, 8, 9])
m = 0
for perm in feedback_phase:
# for perm in init_phase:
    def get_input(i):
        return QUE[i]
    amps = [IntCode(data, get_input, i) for i in range(5)]
    done = False
    QUE = [[perm[i]] for i in range(len(perm))]
    QUE[0].append(0)
    VAL = [0] * len(perm)
    while not done:
        for i, vm in enumerate(amps):
            val = vm.run()
            QUE[i] = []
            if val is None:
                if VAL[-1] > m:
                    m = VAL[-1]
                done = True
                break
            VAL[i] = val
            QUE[(i + 1) % len(QUE)].append(val)
print(m)
