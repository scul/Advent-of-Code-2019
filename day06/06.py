with open('06.in') as f:
    inp = f.read().splitlines()
    data = {}
    for k in inp:
        a, b = k.split(')')
        if a not in data:
            data[a] = []
        data[a].append(b)


def parent(map_, p):
    for k, v in map_.items():
        if p in v:
            return k
    return False


def parents(map_, k):
    ret = []
    p = parent(map_, k)
    while p:
        ret.append(p)
        p = parent(map_, p)
    return ret


def orbits(map_):
    for k, v in map_.items():
        if 'YOU' in v:
            you_parents = parents(map_, k)
        elif 'SAN' in v:
            san_parents = parents(map_, k)
    for i, p in enumerate(you_parents):
        if p in san_parents:
            return i + san_parents.index(p) + 2


print(orbits(data))
