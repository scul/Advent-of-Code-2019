with open('03.in') as f:
    data = f.read().split('\n')

DX = {'D': 0, 'U': 0, 'L': -1, 'R': 1}
DY = {'D': -1, 'U': 1, 'L': 0, 'R': 0}


def move(d):
    current = [0, 0]
    d, n = d[0], int(d[1:])
    if d == 'D':
        current[0] -= n
    elif d == 'U':
        current[0] += n
    elif d == 'R':
        current[1] += n
    elif d == 'L':
        current[1] -= n
    return current


def cross(d1, d2):
    d1, d2 = d1.split(','), d2.split(',')
    points = {}
    px, py, dist = 0, 0, 0
    for d in d1:
        dir, n = d[0], int(d[1:])
        for _ in range(n):
            px += DX[dir] * 1
            py += DY[dir] * 1
            dist += 1
            if (px, py) not in points:
                points[(px, py)] = dist
    cross = []
    px, py, dist = 0, 0, 0
    for d in d2:
        dir, n = d[0], int(d[1:])
        for _ in range(n):
            px += DX[dir] * 1
            py += DY[dir] * 1
            dist += 1
            if (px, py) in points:
                dist1 = points[(px, py)]
                cross.append([px, py, dist, dist1])
    m = min(cross, key=lambda p: p[2]+p[3])
    return m[2]+m[3]


assert cross('R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83') == 610
assert cross('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7') == 410
print(cross(data[0], data[1]))
