import math, operator
from utils.decorators import time_it

with open('10.in') as f:
    data = f.read().split('\n')


def collinear(p1, p2, p3):
    return (p2[0] - p1[0]) * (p3[1] - p1[1]) == (p3[0] - p1[0]) * (p2[1] - p1[1])


def between(p1, p2, p3):
    return p1 <= p3 <= p2 or p2 <= p3 <= p1


def check_blocked(p1, p3, p2):
    if collinear(p1, p2, p3):
        if p1[0] != p2[0]:
            if between(p1[0], p3[0], p2[0]):
                return True
        else:
            if between(p1[1], p3[1], p2[1]):
                return True
    return False


def location(pos_locs):
    print(len(pos_locs))
    max_ = [0]
    for p1 in pos_locs:  # current location
        count = 0
        for p2 in pos_locs:  # can curr see this location
            blocked = False
            for p3 in pos_locs:  # is p2 blocked by p3
                if p1 == p2 or p2 == p3 or p3 == p1:
                    # same points
                    continue
                blocked = check_blocked(p1, p3, p2)
                if blocked:
                    break
            if not blocked:
                count += 1
        if count > max_[0]:
            max_ = [count-1, p1]
    return max_


def get_angle(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    d_x = x2 - x1
    d_y = y2 - y1
    rads = math.atan2(d_x, -d_y)
    degs = math.degrees(rads)
    if degs < 0:
        degs = 360 + degs
    return degs


def vaporize(point, locations):
    # calc angles and dist from point
    points = []
    for loc in locations:
        dist = ((loc[0] - point[0])**2 + (loc[1] - point[1])**2)**0.5
        angle = get_angle(point, loc)
        points.append((loc, angle, dist))

    # sort by angle and dist
    points.sort(key=operator.itemgetter(1, 2), reverse=True)

    # remove
    r = len(points)-1
    p_angle = -1
    count = 0
    while True:
        pt, angle, dist = points[r]
        if angle != p_angle or len(points) <= 1:
            p_pt, _, _ = points.pop(r)
            count += 1
            if count == 200:
                return p_pt[0] * 100 + p_pt[1]
        p_angle = angle
        r -= 1
        if r < 0:
            r = len(points) - 1


pos_locs = []
for y, row in enumerate(data):
    for x, col in enumerate(row):
        if col == '#':
            pos_locs.append((x, y))

# PART 1
# best = location(pos_locs)
# print(best)

#PART 2
best = [292, (20, 20)]   # Part 1 solution
# print(best)
pos_locs.remove(best[1])  # remove best from list of asteroid locations
print(vaporize(best[1], pos_locs))
