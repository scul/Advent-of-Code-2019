from vm import IntCode

with open('13.in') as f:
    data = list(map(int, f.read().split(',')))


def get_input():
    paddle = ball = 0
    for pos, v in screen.items():
        if v == 4:  # Ball
            ball = pos
        elif v == 3:  # Paddle
            paddle = pos
    if paddle[0] < ball[0]:
        return 1  # Move right
    elif paddle[0] > ball[0]:
        return -1  # Move left
    elif paddle[0] == ball[0]:
        return 0  # Don't move


def display(grid):
    max_x = max(x for x, y in grid)
    max_y = max(y for x, y in grid)
    scrn = [[' ' for _ in range(max_x+1)] for _ in range(max_y+1)]
    for k, v in grid.items():
        x, y = k
        scrn[y][x] = tile_ids[v]
    for line in scrn:
        print(''.join(line))


# print(data)
old_data = data[:]
vm = IntCode(data, get_input)
screen = {}
tile_ids = {0: ' ', 1: '|', 2: '#', 3: '=', 4: '@'}
while not vm.halted:
    pos = (vm.run(), vm.run())
    t_id = vm.run()
    if any(i is None for i in [pos, t_id]):
        continue
    screen[pos] = t_id
print(sum(1 for k, v in screen.items() if v == 2))

# display(screen)


old_data[0] = 2
vm2 = IntCode(old_data, get_input)
screen = {}
i = 0
inp = 0
score = 0
while not vm2.halted:
    x, y, t_id = [vm2.run() for _ in range(3)]
    if x == -1 and y == 0:
        score = t_id
    else:
        screen[(x, y)] = t_id

    # display(screen)
print(score)
