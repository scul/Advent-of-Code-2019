from vm import IntCode

with open('17.in') as f:
    data = list(map(int, f.read().split(',')))


def check_intersection(s, x, y):
    delta = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    num_scaff = 0
    y, x = x, y
    for dx, dy in delta:
        if x+dx < 0 or y+dy < 0:
            return False
        try:
            if s[x+dx][y+dy] == '#':
                num_scaff += 1
        except IndexError:
            return False
    # print(num_scaff)
    return num_scaff == 4


def part_one():
    vm = IntCode(data)
    scaffold = [[]]
    i = 0
    while not vm.halted:
        inp = vm.run()
        if inp is None:
            break
        if inp == 10:
            scaffold.append([])
            i += 1
        else:
            scaffold[i].append(chr(inp))
    for line in scaffold:
        print(''.join(line))

    align = 0
    for y, row in enumerate(scaffold):
        for x, char in enumerate(row):
            if scaffold[y][x] == '#' and check_intersection(scaffold, x, y):
                align += x * y
    print(align)


prog = 'A,B,B,A,B,C,A,C,B,C\nL,4,L,6,L,8,L,12\nL,8,R,12,L,12\nR,12,L,6,L,6,L,8\nn\n'


def logic():
    global prog  # Ewww, global variable....
    ret = prog[0]
    prog = prog[1:]
    return [ord(ret)]


def part_two():
    data_copy = data[:]
    data_copy[0] = 2
    vm2 = IntCode(data_copy, logic)
    out = -1
    p_out = -2
    while not vm2.halted:
        p_out = out
        out = vm2.run()
    print(p_out)


# part_one()
part_two()
