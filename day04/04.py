start = 178416
end = 676461


def check(pwd):
    pwd = str(pwd)
    if sorted(pwd) != list(pwd):
        return False
    for c in pwd:
        if pwd.count(c) == 2:
            return True
    return False


assert check(111111) is False
assert check(111123) is False
assert check(111122) is True
assert check(112233) is True
assert check(223450) is False
assert check(123456) is False
assert check(122345) is True
assert check(912456) is False

print(sum(1 for n in range(start, end) if check(n)))
