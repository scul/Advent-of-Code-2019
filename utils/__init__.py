from functools import reduce
from math import gcd


def lcm(a, b):
    return (a * b) // gcd(a, b)


def list_lcm(lst):
    return reduce(lcm, lst)
