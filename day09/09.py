from vm import IntCode

with open('09.in') as f:
    data = list(map(int, f.read().split(',')))

# Day 9 test sequences
# data = [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
# data = [109,19,204,-34,99]
# data = [1102,34915192,34915192,7,4,7,99,0]
# print(int_code(data[:], 1))

# Actual input for day 09
print('Part 1:')
vm1 = IntCode(data, lambda: 1)
print(vm1.run())
print('Part 2:')
vm2 = IntCode(data, lambda: 2)
print(vm2.run())
