from vm import IntCode

with open('05.in') as f:
    data = list(map(int, f.read().split(',')))

for x in [1, 5]:
    vm = IntCode(data, lambda: x)
    while not vm.halted:
        print(vm.run())
