from collections import defaultdict

with open('14.in') as f:
    data = f.read().splitlines()


def split(i):
    n, item = i.split()
    return int(n), item


reactions = {}
inp = {}
for line in data:
    reagents, result = [l.strip() for l in line.split(' => ')]
    reagents = reagents.split(', ')
    reagents = [split(r) for r in reagents]
    amt, result = split(result)
    temp = [amt, reagents]
    reactions[result] = tuple(temp)
    for n, y in reagents:
        if y not in inp:
            inp[y] = 0
        inp[y] += 1
# print(reactions)


def elem_cost(n):
    i = inp.copy()
    i['FUEL'] = 0
    req = {'FUEL': n}
    while True:
        for el in i:
            if i[el] == 0:
                n = req[el]
                if el == 'ORE':
                    return n
                nget, need = reactions[el]
                amt = (nget + n - 1) // nget
                for n_amt, n in need:
                    if n not in req:
                        req[n] = 0
                    req[n] += amt * n_amt
                    i[n] -= 1
                del i[el]
                break


low = 0
high = 1e12
while low < high:
    mid = (low + high + 1) // 2
    print(mid)
    if elem_cost(mid) < int(1e12):
        low = mid
    else:
        high = mid - 1
print(low)