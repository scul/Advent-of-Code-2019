from vm import IntCode
from collections import defaultdict
from random import choice

with open('15.in') as f:
    data = list(map(int, f.read().split(',')))

print(data)
grid = defaultdict(str)


# def get_input():
#     pass

def update(xy, v, dir):
    # dirs = {1: (-1, 0), 2: (1, 0), 3: (0, -1), 4: (0, 1)}
    tx, ty = xy
    dx, dy = dir
    tx += dx
    ty += dy
    if v == 2:
        v = '$'
    elif v == 0:
        v = '#'
    elif v == 1:
        v = '.'
    grid[(tx, ty)] = v
    if v == '#':
        return
    return tx, ty


def display(gg, x, y):
    max_x = max(x for x, y in gg)
    max_y = max(y for x, y in gg)
    min_x = min(x for x, y in gg)
    min_y = min(y for x, y in gg)
    screen = [[' ' for _ in range(min_x, max_x+1)] for _ in range(min_y, max_y+1)]
    for k, v in gg.items():
        tx, ty = k
        # print(tx, ty)
        sym = ' '
        if [tx, ty] == [x, y]:
            # print('d')
            sym = 'D'
        elif [tx, ty] == [0, 0]:
            sym = '@'
        elif grid[tx, ty] == '#':
            sym = '█'
        elif grid[tx, ty] == '.':
            sym = '.'
        if grid[tx, ty] == '$':
            sym = '$'
        # print(ty + abs(min_y), tx + abs(min_x))
        screen[ty+abs(min_y)][tx+abs(min_x)] = sym
    print('------')
    for line in screen:
        print(''.join(line))
    print('------')


x = y = 0
start = (x, y)
grid[(x, y)] = '.'
vm = IntCode(data)
res = -1
d = 1
dirs = {1: (-1, 0), 2: (1, 0), 3: (0, -1), 4: (0, 1)}
m = [1,3,2,4]  # North, West, South, East
mi = 0
while not vm.halted:
    # print('\n(X, Y):', x, y)
    # print('D:     ', d)
    res = vm.run(d)
    # print('res    ', res)
    update((x, y), res, dirs[d])
    # if res == 0:  # Wall
    #     pass
    if res == 1:  # Successful move
        # update((x, y), '.', dirs[d])
        dx, dy = dirs[d]
        x += dx
        y += dy

    elif res == 2:
        display(grid, x, y)
        break
        # found the unit!
        # Update loc
        # Find the min steps from start to loc
    # display(grid, x, y)
    # get new dir to move
    #    TODO: prioritize fwd if unexplored
    #    otherwise unexplored
    #    then backtrack
    poss = []
    pref = []
    blocked = []
    for dir in m:
        tx, ty = x, y
        dx, dy = dirs[dir]
        tx += dx
        ty += dy
        spot = grid[(tx, ty)]
        if spot == '':
            # print('empty')
            pref.append(dir)
        elif spot == '.':
            poss.append(dir)
        elif spot == '#':
            blocked.append(dir)
    # print(f'pref: {pref}')
    # print(f'poss: {poss}')
    if len(blocked) < 3:
        if len(pref) > 0:
            d = choice(pref)
        elif len(poss) > 0:
            # print(poss)
            d = choice(poss)
    else:
        d = choice(poss)
    # print(d)
    # break
    # d = int(input('1: UP\n2: DOWN\n3: LEFT\n4: RIGHT\n'))
    # d = m[dir]

    # if mi >= 20:
    #     break
    # mi += 1
    # if (x, y) == (-2, -4):
    #     break
# display(grid, x, y)