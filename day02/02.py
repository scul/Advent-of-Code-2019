from operator import add, mul

oper = {1: add, 2: mul}

with open('02_input') as f:
    data = list(map(int, f.read().split(',')))


def int_code(seq, inc=4):
    #seq = list(map(int, seq.split(',')))
    i = 0
    while True:
        inst = seq[i]
        if inst == 99:
            return seq
        a, b, store = seq[seq[i+1]], seq[seq[i+2]], seq[i+3]
        seq[store] = oper[inst](a, b)
        i += inc


# assert int_code('1,0,0,0,99') == [2, 0, 0, 0, 99]
# assert int_code('2,3,0,3,99') == [2, 3, 0, 6, 99]
# assert int_code('2,4,4,5,99,0') == [2, 4, 4, 5, 99, 9801]
# assert int_code('1,1,1,4,99,5,6,0,99') == [30, 1, 1, 4, 2, 5, 6, 0, 99]
# assert int_code('1,9,10,3,2,3,11,0,99,30,40,50') == [3500, 9, 10, 70, 2, 3, 11 , 0, 99, 30, 40, 50]
# temp_data = data[:]
# print(int_code(temp_data)[0])

target = 19690720
noun = 0
verb = 0
while True:
    temp_data = data[:]
    temp_data[1] = noun
    temp_data[2] = verb
    temp_data = int_code(temp_data)
    if temp_data[0] == target:
        print(f'solved: {100 * noun + verb}')
        break
    verb += 1
    if verb > 99:
        noun += 1
        verb = 0
        if noun > 99:
            break
